# 3D Printing

## CNC Support (MotorTool)

### Images
![photo_Supp_1](/images/Supp_Motor.stl_2.jpg)
![photo_Supp_2](/images/Supp_Motor.stl_1.jpg)

### 3D Print
- [Support_Motor A](https://gitlab.com/Rootdevel/CNC/blob/aluminium_profiles/Model3D/Print_3D/Supp_Motor_A.stl)
- [Support Motor B](https://gitlab.com/Rootdevel/CNC/blob/aluminium_profiles/Model3D/Print_3D/Supp_Motor_B.stl)

## License
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Licencia Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">CNC Support (MotorTool)</span> por <a xmlns:cc="http://creativecommons.org/ns#" href="https://gitlab.com/Rootdevel/CNC" property="cc:attributionName" rel="cc:attributionURL">Fabian Salamanca (fAnDREs)</a> se distribuye bajo <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Licencia Creative Commons Atribución-CompartirIgual 4.0 Internacional</a>.<br />

### Attributions
See commit details to find the authors of each Part.
- @fandres323
